// There are 52 weeks in a year. Usually.
// There are 52 cards in a deck.
// Let’s generate an alternate sequential reference for weeks.

let weeks = [];

const faces = [2,3,4,5,6,7,8,9,'T','J','Q','K','A']
const suits = ['♣','♦','♥','♠'];

suits.forEach(suit => {
  faces.forEach(face => {
    weeks.push({
      card: face + suit,
      week: '18'+('0'+ (weeks.length + 1)).slice(-2),
    });
  });
});

export default weeks;