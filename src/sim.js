import React from 'react';
import { render, Document, Page, Artboard, Text, View } from 'react-sketchapp';

import data from './data';

const styles = {
  artboard: {
    marginBottom: 50,
    padding: 50,
  },
};

let weeks = [];

data.forEach((w, i) => weeks.push(
  <Artboard key={i} name={w.week} style={styles.artboard}>
    <View><Text>{w.card}</Text></View>
  </Artboard>
));

const Render = () => (
  <Document>
    <Page name="Weeks">
      {weeks}
    </Page>
  </Document>
);

export default () => {
  render(<Render />, context.document.currentPage());
};
